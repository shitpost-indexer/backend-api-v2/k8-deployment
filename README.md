# k8 Deployment

k8 Deployment Resources

# Join the Discord server!

[![Discord](https://discordapp.com/api/guilds/974345893723648061/widget.png?style=banner4)](https://discord.gg/rk8SD7ejGr)

## Deploy

Create redis secret
```kubectl create secret generic redis --from-literal="REDIS_PASS=<password>"```

Deploy to cluster
```kubectl apply -f deployment.yaml```